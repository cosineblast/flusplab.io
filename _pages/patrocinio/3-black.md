---
title:  "Black"
image:  "black"
ref: "black"
categories: "patrocinio.md"
id: 3
---

# Valor: &gt;= R$2200

# Benefícios

* Identificação da marca patrocinadora em todo material de divulgação do evento: online e offline.
* Logo no Crachá.
* Agradecimento e reconhecimento do patrocinador em todas as atividades relacionadas ao evento.
* Oportunidade de exibir banner do patrocinador no evento 
* Possibilidade de apresentação de até 15 minutos do patrocinador na abertura do evento
* Logo da empresa por 1 ano na página de patrocinadores do FLUSP
* Logo da empresa nos vídeos públicados pelo FLUSP durante 1 ano
